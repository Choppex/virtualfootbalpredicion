﻿using System.Linq;
using VirtualFootballPrediction.Models;

namespace VirtualFootballPrediction.Helpers
{
    public static class SeasonHelpers
    {
        public static Season GetActualSeason(this Seasons seasons)
        {
            return seasons.doc[0].data.seasons.FirstOrDefault();
        }
    }
}