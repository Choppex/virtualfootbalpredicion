﻿using System.Linq;
using System.Net;
using Newtonsoft.Json;
using VirtualFootballPrediction.Models;

namespace VirtualFootballPrediction
{
    public static class VirtualFootballService
    {
        private static readonly WebClient Client = new WebClient();
        private const string H2HEndpoint = "https://vgls.betradar.com/vfl/feeds/?/stsvirtuals/pl/Europe:Berlin/gismo/stats_uniquetournament_team_versus/14149";
        private const string SeasonsEndpoint = "https://vgls.betradar.com/vfl/feeds/?/stsvirtuals/pl/Europe:Berlin/gismo/uniquetournament_seasons/14149";
        private const string TableEndpoint = "https://vgls.betradar.com/vfl/feeds/?/stsvirtuals/pl/Europe:Berlin/gismo/stats_season_tables";
        private const string TeamLastMatchesEndpoint = "https://vgls.betradar.com/vfl/feeds/?/stsvirtuals/pl/Europe:Berlin/gismo/stats_uniquetournament_team_lastx/14149";
        
        public static TeamsCompare GetH2HData(int homeId, int awayId, int matchesCount)
        {
            var data = Client.DownloadString(H2HEndpoint + $"/{homeId}/{awayId}/{matchesCount}");
            return JsonConvert.DeserializeObject<TeamsCompare>(data);
        }

        public static Seasons GetSeasons()
        {
            var data = Client.DownloadString(SeasonsEndpoint);
            return JsonConvert.DeserializeObject<Seasons>(data);
        }

        public static Tables GetTable(int seasonId)
        {
            var data = Client.DownloadString(TableEndpoint +$"/{seasonId}/0");
            return JsonConvert.DeserializeObject<Tables>(data);
        }
        public static TeamLastMatches GetLastTeamMatches(int teamId, int count)
        {
            var data = Client.DownloadString(TeamLastMatchesEndpoint +$"/{teamId}/{count}");
            return JsonConvert.DeserializeObject<TeamLastMatches>(data);
        }
    }
}