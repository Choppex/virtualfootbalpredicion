﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ModernWpf;
using VirtualFootballPrediction.Helpers;
using VirtualFootballPrediction.Models;

namespace VirtualFootballPrediction
{
    public class MatchesData
    {
        public string FirstTeam { get; set; }
        public string Score { get; set; }
        public string SecondTeam { get; set; }
    }
    public partial class MainWindow
    {
        public TeamNames FirstTeam { get; set; }
        public TeamNames SecondTeam { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            FirstTeamPicker.ItemsSource = TeamNames.Teams;
            SecondTeamPicker.ItemsSource = TeamNames.Teams;
            TeamLastMatchesPicker.ItemsSource = TeamNames.Teams;
        }

        private void DownloadButtonClick(object sender, RoutedEventArgs e)
        {
            var count = Convert.ToInt32(MatchesCount.Text) > 100 ? 100 : Convert.ToInt32(MatchesCount.Text);
            var teamsMatches = VirtualFootballService.GetH2HData(FirstTeam.Id, SecondTeam.Id, count);
            var displayMatches = new List<MatchesData>();
            foreach (var match in teamsMatches.doc.First().data.matches)
            {
                displayMatches.Add(new MatchesData()
                {
                    FirstTeam = match.teams.home.name,
                    SecondTeam= match.teams.away.name,
                    Score = match.result.home + " : " + match.result.away
                });
            }

            DataGridMatches.ItemsSource = displayMatches;
            var firstAway = teamsMatches.doc.First().data.matches.Select(n => n.teams.away.name == FirstTeam.Name && n.result.winner == "away").Count(c => c);
            var firstHome = teamsMatches.doc.First().data.matches.Select(n => n.teams.home.name == FirstTeam.Name && n.result.winner == "home").Count(c => c);
            var secondAway = teamsMatches.doc.First().data.matches.Select(n => n.teams.away.name == SecondTeam.Name && n.result.winner == "away").Count(c => c);
            var secondHome = teamsMatches.doc.First().data.matches.Select(n => n.teams.home.name == SecondTeam.Name && n.result.winner == "home").Count(c => c);
            var draw = teamsMatches.doc.First().data.matches.Count( n=>n.result.winner is null);
            
            FirstTeamWinsName.Content = FirstTeam.Name;
            FirstTeamWinsAway.Content = "Wygrane na wyjeździe: " + firstAway;
            FirstTeamWinsHome.Content = "Wygrane u siebie: " + firstHome;
            var total = firstAway + firstHome;
            FirstTeamWinsTotal.Content = "Wygrane mecze: " + total + "/" + count;
            SecondTeamWinsName.Content = SecondTeam.Name;
            SecondTeamWinsAway.Content = "Wygrane na wyjeździe: " + secondAway;
            SecondTeamWinsHome.Content = "Wygrane u siebie: " + secondHome;
            total = secondAway + secondHome;
            SecondTeamWinsTotal.Content = "Wygrane mecze: " + total + "/" + count;
            Draw.Content = "Remisy: " + draw;
        }

        private void FirstTeamChanged(object sender, SelectionChangedEventArgs e)
        {
            FirstTeam = (((ComboBox) sender).SelectedItem as TeamNames);
            DownloadButton.IsEnabled = FirstTeam != SecondTeam;
        }

        private void SecondTeamChanged(object sender, SelectionChangedEventArgs e)
        {
            SecondTeam = (((ComboBox) sender).SelectedItem as TeamNames);
            DownloadButton.IsEnabled = FirstTeam != SecondTeam;
        }

        private void ThemeToggle_OnClick(object sender, RoutedEventArgs e)
        {
            if (ThemeToggle.IsChecked == true)
            {
                ThemeManager.Current.ApplicationTheme = ApplicationTheme.Dark;
                ThemeToggle.Content = "Ciemny motyw";
            }
            else
            {
                ThemeManager.Current.ApplicationTheme = ApplicationTheme.Light;
                ThemeToggle.Content = "Jasny motyw";

            }
        }

        private void DownloadSeasonClick(object sender, RoutedEventArgs e)
        {
            var id = VirtualFootballService.GetSeasons().GetActualSeason()._id;
            CurrentSeason.Text = id;
            var tables = VirtualFootballService.GetTable(Convert.ToInt32(id));
            var displayTable = new List<TableDisplayModel>();
            foreach (var tableRow in tables.doc.First().data.tables.First().tablerows)
            {
                displayTable.Add(new TableDisplayModel()
                {
                    TeamName = tableRow.team.name,
                    Wins = tableRow.winTotal,
                    Draws = tableRow.drawTotal,
                    Loses = tableRow.lossTotal,
                });
            }

            DataGridTable.ItemsSource = displayTable;
        }

        private void DownloadLastMatchesClick(object sender, RoutedEventArgs e)
        {
            var team = TeamLastMatchesPicker.SelectionBoxItem as TeamNames;
            var matches = VirtualFootballService.GetLastTeamMatches(team.Id, Convert.ToInt32(LastXMatches.Text));
            var display = new List<MatchesData>();
            foreach (var match in matches.doc.First().data.matches)
            {
                display.Add(new MatchesData()
                {
                    FirstTeam = match.teams.home.name,
                    SecondTeam = match.teams.away.name,
                    Score = match.result.home + " : " + match.result.away
                });
            }
            var away = matches.doc.First().data.matches.Select(n => n.teams.away.name == team.Name && n.result.winner == "away").Count(c => c);
            var home = matches.doc.First().data.matches.Select(n => n.teams.home.name == team.Name && n.result.winner == "home").Count(c => c);
            var draw = matches.doc.First().data.matches.Count( n=>n.result.winner is null);

            TeamName.Content = team.Name;
            TeamDraw.Content = "Remisy: " + draw;
            TeamWinsAway.Content = "Wygrane na wyjeździe: " + away + "/" + LastXMatches.Text;
            TeamWinsHome.Content = "Wygrane u siebie:" + home + "/" + LastXMatches.Text;
            TeamWinsTotal.Content = "Wygrane mecze: " + (away + home) + "/" + LastXMatches.Text;
            DataGridLastMatches.ItemsSource = display;
        }
    }
}