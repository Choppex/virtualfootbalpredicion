﻿using System.Collections.Generic;

namespace VirtualFootballPrediction.Models
{
    public class Tournament
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public int _isk { get; set; }
        public int _tid { get; set; }
        public int _utid { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public object ground { get; set; }
        public bool friendly { get; set; }
        public int seasonid { get; set; }
        public int currentseason { get; set; }
        public string year { get; set; }
        public string seasontype { get; set; }
        public string seasontypename { get; set; }
        public string seasontypeunique { get; set; }
        public int livetable { get; set; }
        public object cuprosterid { get; set; }
        public bool roundbyround { get; set; }
        public object tournamentlevelorder { get; set; }
        public object tournamentlevelname { get; set; }
        public bool outdated { get; set; }
    }

    public class Realcategory
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public string name { get; set; }
        public object cc { get; set; }
    }

    public class Rules
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public string name { get; set; }
    }

    public class Tablerow
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public string changeTotal { get; set; }
        public int changeHome { get; set; }
        public int changeAway { get; set; }
        public int drawTotal { get; set; }
        public int drawHome { get; set; }
        public int drawAway { get; set; }
        public int goalDiffTotal { get; set; }
        public int goalDiffHome { get; set; }
        public int goalDiffAway { get; set; }
        public int goalsAgainstTotal { get; set; }
        public int goalsAgainstHome { get; set; }
        public int goalsAgainstAway { get; set; }
        public int goalsForTotal { get; set; }
        public int goalsForHome { get; set; }
        public int goalsForAway { get; set; }
        public int lossTotal { get; set; }
        public int lossHome { get; set; }
        public int lossAway { get; set; }
        public int total { get; set; }
        public int home { get; set; }
        public int away { get; set; }
        public int pointsTotal { get; set; }
        public int pointsHome { get; set; }
        public int pointsAway { get; set; }
        public int pos { get; set; }
        public int posHome { get; set; }
        public int posAway { get; set; }
        public int sortPositionTotal { get; set; }
        public Team team { get; set; }
        public int winTotal { get; set; }
        public int winHome { get; set; }
        public int winAway { get; set; }
    }

    public class Matchtype
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int settypeid { get; set; }
        public string column { get; set; }
    }

    public class Tabletype
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public string column { get; set; }
    }

    public class Set
    {
        public int id { get; set; }
        public string name { get; set; }
        public List<int> headers { get; set; }
    }

    public class Header
    {
        public int id { get; set; }
        public string name { get; set; }
        public string column { get; set; }
        public string tooltip { get; set; }
        public string datapriority { get; set; }
    }

    public class Table
    {
        public string _doc { get; set; }
        public string _id { get; set; }
        public object parenttableid { get; set; }
        public object leaguetypeid { get; set; }
        public List<object> parenttableids { get; set; }
        public string seasonid { get; set; }
        public int maxrounds { get; set; }
        public int currentround { get; set; }
        public int presentationid { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public object groupname { get; set; }
        public Tournament tournament { get; set; }
        public Realcategory realcategory { get; set; }
        public Rules rules { get; set; }
        public int totalrows { get; set; }
        public List<Tablerow> tablerows { get; set; }
        public List<Matchtype> matchtype { get; set; }
        public List<Tabletype> tabletype { get; set; }
        public List<Set> set { get; set; }
        public List<Header> header { get; set; }
    }

    public class DataTables
    {
        public string _id { get; set; }
        public string _doc { get; set; }
        public int _utid { get; set; }
        public int _sid { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
        public bool neutralground { get; set; }
        public bool friendly { get; set; }
        public int currentseasonid { get; set; }
        public string year { get; set; }
        public List<Table> tables { get; set; }
    }
    
    public class Tables
    {
        public string queryUrl { get; set; }
        public List<DocTables> doc { get; set; }
    }
    
    public class DocTables
    {
        public string @event { get; set; }
        public int _dob { get; set; }
        public int _maxage { get; set; }
        public DataTables data { get; set; }
    }


}