﻿using System.Collections.Generic;

namespace VirtualFootballPrediction.Models
{
    public class TeamNames
    {
        public string Name { get; set; }
        public int Id { get; set; }

        private static TeamNames New(string name, int id)
        {
            return new TeamNames()
            {
                Id = id,
                Name = name
            };
        }

        public static readonly TeamNames ErkaChorzow = TeamNames.New("Erka Chorzów", 276502);
        public static readonly TeamNames PasyKrakow = TeamNames.New("Pasy Kraków", 276513);
        public static readonly TeamNames ElkaWarszawa = TeamNames.New("Elka Warszawa", 276507);
        public static readonly TeamNames WiaraPoznan = TeamNames.New("Wiara Poznań", 276510);
        public static readonly TeamNames BialaGwiazda = TeamNames.New("Biała Gwiazda", 276505);
        public static readonly TeamNames LewGdansk = TeamNames.New("Lew Gdańsk", 276508);
        public static readonly TeamNames MiedziowiLubin = TeamNames.New("Miedziowi Lubin", 276501);
        public static readonly TeamNames WojskoWroclaw = TeamNames.New("Wojsko Wrocław", 276512);
        public static readonly TeamNames MKSSosnowiec = TeamNames.New("MKS Sosnowiec", 276503);
        public static readonly TeamNames NaprzodRzeszow = TeamNames.New("Naprzód Rzeszów", 276515);
        public static readonly TeamNames TorcidaZabrze = TeamNames.New("Torcida Zabrze", 276506);
        public static readonly TeamNames CzerwoniLodz = TeamNames.New("Czerwoni Łódź", 276514);
        public static readonly TeamNames JagaBialystok = TeamNames.New("Jaga Białystok", 276511);
        public static readonly TeamNames NaftaPlock = TeamNames.New("Nafta Płock", 276516);
        public static readonly TeamNames LodzianieKS = TeamNames.New("Łodzianie KS", 276504);
        public static readonly TeamNames KSLublin = TeamNames.New("KS Lublin", 276509);

        public static IReadOnlyList<TeamNames> Teams = new List<TeamNames>()
        {
            ErkaChorzow,
            ElkaWarszawa,
            WiaraPoznan,
            BialaGwiazda,
            LewGdansk,
            MiedziowiLubin,
            WojskoWroclaw,
            MKSSosnowiec,
            NaprzodRzeszow,
            TorcidaZabrze,
            CzerwoniLodz,
            JagaBialystok,
            NaftaPlock,
            LodzianieKS,
            KSLublin,
            PasyKrakow
        };
    }
}