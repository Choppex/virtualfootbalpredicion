﻿namespace VirtualFootballPrediction.Models
{
    public class TableDisplayModel
    {
        public string TeamName { get; set; }
        public int Wins { get; set; }
        public int Draws { get; set; }
        public int Loses { get; set; }
    }
}