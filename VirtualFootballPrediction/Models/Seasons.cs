﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace VirtualFootballPrediction.Models
{
    public class Start
    {
        public string _doc { get; set; }
        public string time { get; set; }
        public string date { get; set; }
        public string tz { get; set; }
        public int tzoffset { get; set; }
        public int uts { get; set; }
    }

    public class End
    {
        public string _doc { get; set; }
        public string time { get; set; }
        public string date { get; set; }
        public string tz { get; set; }
        public int tzoffset { get; set; }
        public int uts { get; set; }
    }

    public class Roundname
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int name { get; set; }
    }

    public class Season
    {
        public string _id { get; set; }
        public string _doc { get; set; }
        public int _utid { get; set; }
        public int _sid { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public Start start { get; set; }
        public End end { get; set; }
        public bool neutralground { get; set; }
        public bool friendly { get; set; }
        public int currentseasonid { get; set; }
        public string year { get; set; }
        public bool current { get; set; }
        public int matches { get; set; }
        public Roundname roundname { get; set; }
    }

    public class DataSeasons
    {
        public List<Season> seasons { get; set; }
    }

    public class DocSeasons
    {
        public string @event { get; set; }
        public int _dob { get; set; }
        public int _maxage { get; set; }
        [JsonProperty("data")]
        public DataSeasons data { get; set; }
    }

    public class Seasons
    {
        public string queryUrl { get; set; }
        [JsonProperty("doc")]
        public List<DocSeasons> doc { get; set; }
    }
}