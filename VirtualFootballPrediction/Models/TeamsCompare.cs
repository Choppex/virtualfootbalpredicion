﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace VirtualFootballPrediction
{
    public class TeamsCompare
    {
        public string queryUrl { get; set; }
        public List<Doc> doc { get; set; }
    }
    public class Time
    {
        public string _doc { get; set; }
        public string time { get; set; }
        public string date { get; set; }
        public string tz { get; set; }
        public int tzoffset { get; set; }
        public int uts { get; set; }
    }

    public class Roundname
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int name { get; set; }
    }

    public class Result
    {
        public int home { get; set; }
        public int away { get; set; }
        public string period { get; set; }
        public string winner { get; set; }
    }

    public class Score
    {
        public int home { get; set; }
        public int away { get; set; }
    }

    public class Periods
    {
        public Score Score1 { get; set; }
        public Score Score2 { get; set; }
    }
    
    public class PlayingTeam
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int uid { get; set; }
        public string name { get; set; }
        public string mediumname { get; set; }
        public string abbr { get; set; }
        public object nickname { get; set; }
        public bool iscountry { get; set; }
        public bool haslogo { get; set; }
    }

    public class Teams
    {
        public PlayingTeam home { get; set; }
        public PlayingTeam away { get; set; }
        public Team hm { get; set; }
        public Team aw { get; set; }
    }

    public class Match
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public int _tid { get; set; }
        public int _utid { get; set; }
        public Time time { get; set; }
        public int round { get; set; }
        public Roundname roundname { get; set; }
        public string week { get; set; }
        public Result result { get; set; }
        public Periods periods { get; set; }
        public int _seasonid { get; set; }
        public Teams teams { get; set; }
        public bool neutralground { get; set; }
        public object comment { get; set; }
        public object status { get; set; }
        public bool tobeannounced { get; set; }
        public bool postponed { get; set; }
        public bool canceled { get; set; }
        public bool inlivescore { get; set; }
        public int stadiumid { get; set; }
        public object bestof { get; set; }
        public bool walkover { get; set; }
        public bool retired { get; set; }
        public bool disqualified { get; set; }
    }

    public class Uniquetournament
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _utid { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public string name { get; set; }
        public int currentseason { get; set; }
        public bool friendly { get; set; }
    }

    public class Tournament
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public int _isk { get; set; }
        public int _tid { get; set; }
        public int _utid { get; set; }
        public string name { get; set; }
        public string abbr { get; set; }
        public object ground { get; set; }
        public bool friendly { get; set; }
        public int seasonid { get; set; }
        public int currentseason { get; set; }
        public string year { get; set; }
        public string seasontype { get; set; }
        public string seasontypename { get; set; }
        public string seasontypeunique { get; set; }
        public int livetable { get; set; }
        public object cuprosterid { get; set; }
        public bool roundbyround { get; set; }
        public object tournamentlevelorder { get; set; }
        public object tournamentlevelname { get; set; }
        public bool outdated { get; set; }
    }

    public class Tournaments
    {
        public Tournament Tournament { get; set; }
    }

    public class Team
    {
        public string _doc { get; set; }
        public int _id { get; set; }
        public int _sid { get; set; }
        public int _rcid { get; set; }
        public string name { get; set; }
        public string mediumname { get; set; }
        public object suffix { get; set; }
        public string abbr { get; set; }
        public object nickname { get; set; }
        public int teamtypeid { get; set; }
        public bool iscountry { get; set; }
        public string sex { get; set; }
        public bool haslogo { get; set; }
        public object founded { get; set; }
        public object website { get; set; }
        public string @base { get; set; }
        public string sleeve { get; set; }
        public string number { get; set; }
        public bool real { get; set; }
        public string type { get; set; }
    }

    public class Jersey
    {
        public Team home { get; set; }
        public Team away { get; set; }
    }

    public class Data
    {
        public List<Match> matches { get; set; }
        public Uniquetournament uniquetournament { get; set; }
        public Tournaments tournaments { get; set; }
        public Teams teams { get; set; }
        public Jersey jersey { get; set; }
    }

    public class Doc
    {
        public string @event { get; set; }
        public int _dob { get; set; }
        public int _maxage { get; set; }
        public Data data { get; set; }
    }
    
}